# ansible-playbook-lamp
[Ansible] playbook for LAMP setup

## Tunables
### all
* `php_version`: (string) - PHP version e.g. 5.6, 7.0, 7.1, 7.2
* `php_webserver_daemon`: (string) - web server to use with PHP e.g. *default* httpd (apache2), nginx
* `website_repo`: (string) - This Git repo i.e. "git@github.com:poundandgrain/example.git"

### group_vars/environment
* `server_name`: (string) - web site domain name
* `mysql_root_password`: (string) - MySQL root password
* `mysql_root_password_update`: (boolean) - Update MySQL root password with `mysql_root_password`?

## Notes
* docroot is `/var/www/{{ server_name }}`

## Usage
### Prerequisites
* user "ubuntu" has been set up on guest machine with *authorized_keys*, *sudo* privileges and member of group `www-data`
* basic hardening done with [5Minutes Server Security Essentials]

### Example
* `ansible-galaxy install -r requirements.yml`
* `ansible-playbook server.yml -i hosts/development -u ubuntu -K`

[Ansible]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
[5Minutes Server Security Essentials]: https://github.com/chhantyal/5minutes
